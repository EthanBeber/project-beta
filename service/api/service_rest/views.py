from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Technician, Appointment
from .encoders import  TechnicianEncoder, AppointmentEncoder


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(employee_number=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(employee_number=pk)
            technician.delete()
            return JsonResponse(
                {"message": "Technician has been deleted"}

            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(employee_number=pk)
            props =["name"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_number=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )
     # if any input vin matches the vin of an automobile object in inventory set vip to true
        if len(AutomobileVO.objects.filter(vin=content["vin"])) > 0:
            content["vip"] = True
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,)






@require_http_methods(["DELETE", "GET", "PUT", "PATCH"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"message": "The appointment has been deleted"}
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not Exist"})
    elif request.method == "PATCH":
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)
            content["completed"] = True
            props = ["completed"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            response =JsonResponse({"message": "Service appointment completed"}, safe = False)
            return response
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not Exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)
            technician = Technician.objects.get(employee_number=content["technician"])
            content["technician"] = technician
            props = ["owner", "date", "time", "technician", "service"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not Exist"})
            response.status_code = 404
            return response
