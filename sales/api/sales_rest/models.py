from django.db import models
from django.core.validators import MinLengthValidator

class AutomobileVO(models.Model):
    model = models.CharField(max_length=100)
    year = models.PositiveSmallIntegerField()
    color = models.CharField(max_length=50)
    vin = models.CharField(max_length=17)
    manufacturer = models.CharField(max_length=50)
    sold = models.BooleanField(default=False)
    
    def sell(self):
        self.sold = True
        self.save()
        
    def __str__(self):
        return f"{self.year} {self.model} ({self.color})"



class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employee_num = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

class Customer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    phone = models.CharField(
        max_length=10,
        validators=[
            MinLengthValidator(10, 'the field must contain 10 characters')
            ])
    
    def __str__(self):
        return f"{self.name}"
    
class Sale(models.Model):
    automobile = models.ForeignKey(
        "AutomobileVO",
        related_name="sale",
        on_delete=models.PROTECT,
        null=True)
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.PROTECT,
        null=True)
    customer = models.ForeignKey(
        "Customer",
        related_name="purchases",
        on_delete=models.PROTECT,
        null=True)
    price = models.IntegerField()
    
    def __str__(self):
        return f"{self.automobile.year} {self.automobile.model}"
