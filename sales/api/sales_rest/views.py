from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, SalesPerson, Customer, Sale
from .encoders import AutomobileEncoder, SaleEncoder, SaleListEncoder, SalesPerson, SalesPersonEncoder, CustomerEncoder

@require_http_methods(["GET"])
def api_autos(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": autos},
            encoder=AutomobileEncoder,
        )
    else:
        response = JsonResponse({"message": "Bad request"})
        response.status_code = 400
        return response

@require_http_methods(["GET"])
def api_autos_available(request):
    autos = AutomobileVO.objects.filter(sold=False)
    return JsonResponse(
            {"automobiles": autos},
            encoder=AutomobileEncoder,
        )
    

@require_http_methods(["POST","GET","DELETE","PUT"])
def api_sales_person(request,id=None):
    if request.method == "POST":
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    elif request.method == "GET" and id==None:
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder,
        )
    elif request.method == "GET" and id != None:      
        try:
            sales_person = SalesPerson.objects.get(id=id)
            return JsonResponse(
               sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales Person does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE" and id != None:
        count, _ = SalesPerson.objects.filter(id=id).delete()  
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT" and id != None:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=id)
            properties = ["name", "employee_num"]
            for prop in properties:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales Person does not exist"})
            response.status_code = 404
            return response
    else:
        response = JsonResponse({"message": "Bad request"})  # HERE ==================  HERE
        response.status_code = 400
        return response

@require_http_methods(["GET"])
def api_sales_person_sales(request,id):
    if request.method == "GET" and id != None:        # THIS IS WHAT I AM WORKING ON ========================
        try:
            sales_person_sales = SalesPerson.objects.get(id=id).sales.all()
            return JsonResponse(
                {"sales": sales_person_sales},
                encoder=SaleEncoder,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales Person does not exist"})
            response.status_code = 404
            return response
    else:
        response = JsonResponse({"message": "Bad request"})
        response.status_code = 400
        return response

@require_http_methods(["POST","GET"])
def api_customers(request):
    if request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        response = JsonResponse({"message": "Bad request"})
        response.status_code = 400
        return response

@require_http_methods(["POST","GET"])
def api_sales(request):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            customer_id = content["customer_id"]
            content["customer"] = Customer.objects.get(id=customer_id)
            employee_num = content["employee_num"]
            content["sales_person"] = SalesPerson.objects.get(employee_num = employee_num)
            automobile_vin = content["automobile_vin"]
            auto_inst = AutomobileVO.objects.get(vin=automobile_vin)
            if auto_inst.sold == True:
                response = JsonResponse({"message": "Cannot sell a car that has already been sold."})
                response.status_code = 405
                return response
            content["automobile"] = auto_inst
            content.pop("customer_id")
            content.pop("employee_num")
            content.pop("automobile_vin")
            sale = Sale.objects.create(**content)
            auto_inst.sell()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except (SalesPerson.DoesNotExist, Customer.DoesNotExist, AutomobileVO.DoesNotExist):
            response = JsonResponse({"message": "Selected object does not exist"})
            response.status_code = 404
            return response
        
    elif request.method == "GET":
        sales = Sale.objects.all().order_by("sales_person")
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
        )
    else:
        response = JsonResponse({"message": "Bad request"})
        response.status_code = 400
        return response
