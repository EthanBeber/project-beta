# Generated by Django 4.0.3 on 2022-12-09 21:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0009_alter_customer_phone_alter_salesperson_employee_num'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='automobilevo',
            name='imp_href',
        ),
        migrations.RemoveField(
            model_name='automobilevo',
            name='sold',
        ),
    ]
