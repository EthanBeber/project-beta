# Generated by Django 4.0.3 on 2022-12-07 23:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0007_alter_sale_sales_person'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesperson',
            name='employee_num',
            field=models.PositiveIntegerField(max_length=20, unique=True),
        ),
    ]
