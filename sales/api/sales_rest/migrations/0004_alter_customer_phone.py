# Generated by Django 4.0.3 on 2022-12-07 15:40

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_automobilevo_sold'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='phone',
            field=models.CharField(max_length=10, validators=[django.core.validators.MinLengthValidator(10, 'the field must contain at least 9 characters')]),
        ),
    ]
