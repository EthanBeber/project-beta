from django.contrib import admin

# Register your models here.

from .models import AutomobileVO, SalesPerson, Customer, Sale


@admin.register(AutomobileVO)
class AutoAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass