# CarCar

Team:

* Ethan Beber - Service (all), Inventory (create manufacturer, list manufacturers, list models)
* Michel Menin - Sales (all), Inventory (create Vehicle Model, Show List of Autos, Create Auto in inventory)

## Design

## Service microservice


The service api contains three models, one for a technician, one for service appointments, and an automobileVO model that syncs vin numbers from the inventory to cross reference vin numbers from service appointments and assign a vip status to a customer if the vin matches one rom inventory. The technician model provides fields for a user on the front end to add a technician. The appointment model does the same, provides fields for a user to create a service appointment from the front end and has a foreign key relationship with the technician model so that a user can select an existing technician and assign them to a service appointment. It also has a field to assign a completed status to the appointment which will determine where the appointment instance is stored on the front end for user access, in a list of service appointments if not completed and in a service history organized by vin number if it is completed.

## Sales microservice

The poller pulls information from the inventory microservice into the sales microservice AutomobileVO model.  This allows to have a local copy of the inventory vehicles from the inventory microservice locally in the sales microservice with enough information to identify the vehicle correctly when registering a sale.  There are other models in place: Sales Person, Customer and Sale.  The Sale model records sales transactions making relations with all other models.  The information recorded is: who bought the car, who sold the car, which car, and how much was paid for it.  The microservice automobileVO has an additional property to identify if the vehicle has already been sold to avoid registering a vehicle to two different sales (selling a car that has already been sold), also filtering the list of vehicles that may be selected to register the sale.  This microservice does not modify the data in the inventory microservice.
