import React, {useState, useEffect} from 'react';

function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState ([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        const data = await response.json()

        setManufacturers(data.manufacturers)
    }

    useEffect(() => {
        fetchData ();
    }, [])

    return (
    <>
    <div className="px-4 py-5 my-1 text-left">
    <h1 className="display-5 fw-bold">Manufacturers</h1>
    </div>
     <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            {manufacturers.map(manufacturer => {
                return (
                    <tr key={manufacturer.id}>
                        <td>{manufacturer.name}</td>
                    </tr>
                )
            })}
        </tbody>
    </table>
    </>
   )
}

export default ManufacturersList;
