import React, { useEffect,useState   } from 'react';


function ModelForm(){
    const [manufacturers,setManufacturers]=useState([]);
    const [manufacturerId,setManufac]=useState('');
    const [model,setModel]=useState('');
    const handleModelChange = (e) =>{ setModel(e.target.value); }
    const handleManufacChange = (e) =>{ setManufac(e.target.value); }

    const loadManufacturers = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
          let data = await response.json();
          setManufacturers( data.manufacturers.map( (manufac) => ({
              id: manufac.id,
              name: manufac.name
              }))
          );
        }
        else if (!response.ok) {
          console.log("Error retrieving manufacturer list.")
        }
      }

    useEffect( () => {
        loadManufacturers();
        }, [] );

    const submitForm = async () => {
        let data = {
            manufacturer_id: manufacturerId,
            name: model
        }

        const modelUrl ='http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
    const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            setManufac('');
            setModel('');
        }
        else {console.log("Unable to create model record")}
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1> Add a Model</h1>
                <div className="form-floating mb-3">
                    <input onChange={handleModelChange} value={model} placeholder="model" required type="text" name="model" id="model" className="form-control" />
                    <label htmlFor="model">Model name</label>
                </div>
                <div className="mb-3">
                <select onChange={handleManufacChange} required name="manufacturerId"
                        id="manufacturerId" className='form-select' value={manufacturerId}>
                    <option value="">Choose a manufacturer</option>
                    {manufacturers?.map(manufac => {
                        return (
                        <option value={manufac.id} key={manufac.id}>
                            {manufac.name}
                        </option>
                        );
                    })}
                </select>
            </div>
                <button onClick={submitForm} className="btn btn-primary">Create</button>
            </div>
            </div>
        </div>
    )

}

export default ModelForm;
