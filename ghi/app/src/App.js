import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ListManufacturers';
import ModelsList from './ListModels';
import ManufacturerForm from './AddManufacturer';
import ModelForm from './AddModel';
import TechnicianForm from './AddTechnician';
import AppointmentForm from './AddAppointment';
import ServiceAppointments from './ServiceAppointments';
import ServiceHistory from './ServiceHistory';
import ListAutos from './ListAutos';
import AutoForm from './AddAuto';
import SalesPersonForm from './AddSalesP';
import CustomerForm from './AddCustomer';
import SaleForm from './AddSale';
import PersonSales from './PersonSales';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path = "/manufacturers" element={ <ManufacturersList />} />
          <Route path = "/manufacturers/new" element={<ManufacturerForm />} />
          <Route path = "/models" element={<ModelsList />} />
          <Route path = "/models/new" element={<ModelForm />} />
          <Route path = "/technicians/new" element={<TechnicianForm />} />
          <Route path = "/appointments/new" element={<AppointmentForm />} />
          <Route path = "/appointments" element={<ServiceAppointments />} />
          <Route path = "/service-history" element={<ServiceHistory />} />
          <Route path = "/automobiles" element={<ListAutos />} />
          <Route path = "/automobiles/new" element={<AutoForm />} />
          <Route path = "/salespersons/new" element={<SalesPersonForm />} />
          <Route path = "/customers/new" element={<CustomerForm />} />
          <Route path = "/sales/new" element={<SaleForm />} />
          <Route path = "/sales/person" element={<PersonSales />} />
          <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
