import React, { useState } from 'react';

function SalesPersonForm(){
    const[name,setName] = useState('');
    const[employee_num,setNum] = useState(0);
    const handleNameChange = (e) =>{ setName(e.target.value); }
    const handleNumChange = (e) =>{ setNum(e.target.value); }

    const submitForm = async () => {
        let data = {
            name: name,
            employee_num: employee_num
        }

        const salespUrl ='http://localhost:8090/api/salespersons/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salespUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setNum(0); }
        else {console.log("Unable to create sales person record")}
      }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1> Add a Sales Person</h1>
                <p></p>
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="name"
                            required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleNumChange} value={employee_num} placeholder="employee_num"
                            required type="number" name="employee_num" id="employee_num" className="form-control" />
                    <label htmlFor="employee_num">Employee number</label>
                </div>
                <button onClick={submitForm} className="btn btn-primary">Create</button>
            </div>
            </div>
        </div>
    )

}

export default SalesPersonForm;
