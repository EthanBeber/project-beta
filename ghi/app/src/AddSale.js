import React, { useState, useEffect, } from 'react';


function SaleForm(){
    const [salesPersons,setPersons]=useState([]);
    const [autos,setAutos]=useState([]);
    const [customers,setCustomers]=useState([]);
    const [price,setPrice]=useState(0);
    const [employeeNum,setPerson]=useState('');
    const [customerId,setCustomer]=useState('');
    const [vin,setVin]=useState('');

    const handleAutoChange = (e) =>{ setVin(e.target.value); }
    const handlePersonChange = (e) =>{ setPerson(e.target.value); }
    const handleCustomerChange = (e) =>{ setCustomer(e.target.value); }
    const handlePriceChange = (e) =>{ setPrice(e.target.value); }

    const loadCustomers = async () => {   
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
          let data = await response.json();
          setCustomers( data.customers.map( (customer) => ({
              name: customer.name,
              id: customer.id
              }))
          );
        }
        else if (!response.ok) {
          console.log("Error retrieving customer list.")
        }
      }
      const loadPersons = async () => {   
        const response = await fetch('http://localhost:8090/api/salespersons/');
        if (response.ok) {
          let data = await response.json();
          setPersons( data.sales_persons.map( (person) => ({
              name: person.name,
              employeeNum: person.employee_num,
              id: person.id
              }))
          );
        }
        else if (!response.ok) {
          console.log("Error retrieving sales persons list.")
        }
      }
    const loadAutos = async () => {   
        const response = await fetch('http://localhost:8090/api/autos/available/');
        if (response.ok) {
          let data = await response.json();
          setAutos( data.automobiles.map( (auto) => ({
              model: auto.model,
              vin: auto.vin,
              color: auto.color,
              year: auto.year
              }))
          );
        }
        else if (!response.ok) {
          console.log("Error retrieving automobile list.")
        }
      }

    const submitForm = async () => {   
        let data = {
            automobile_vin: vin,
            customer_id: customerId,
            employee_num: employeeNum,
            price: price
        }
        
        const saleUrl ='http://localhost:8090/api/sales/new/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            loadAutos();
            setCustomer('');
            setPerson('');
            setPrice(0); }
        else {console.log("Unable to create sales record")}
      }
    
    useEffect( () => {
        loadPersons();
        loadAutos();
        loadCustomers();
      }, [] );


    return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1> Add a Sale Record</h1>

            <div className="mb-3">  
                <select onChange={handleAutoChange} required name="vin"
                        id="vin" className='form-select' value={vin}>
                <option value="">Choose an automobile</option>
                {autos?.map(auto => {
                    return (
                    <option value={auto.vin} key={auto.vin}>
                        {auto.vin} / {auto.year} {auto.model} / {auto.color}
                    </option>
                    );
                })}
                </select>
            </div>

            <div className="mb-3">  
                <select onChange={handleCustomerChange} required name="customerId"
                        id="customerId" className='form-select' value={customerId}>
                <option value="">Choose a customer</option>
                {customers?.map(customer => {
                    return (
                    <option value={customer.id} key={customer.id}>
                        {customer.name}
                    </option>
                    );
                })}
                </select>
            </div>

            <div className="mb-3">  
                <select onChange={handlePersonChange} required name="employeeNum"
                        id="employeeNum" className='form-select' value={employeeNum}>
                <option value="">Choose a Sales Person</option>
                {salesPersons?.map(person => {
                    return (
                    <option value={person.employeeNum} key={person.employeeNum}>
                        {person.name} / No. {person.employeeNum}
                    </option>
                    );
                })}
                </select>
            </div>

            <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="price" required type="number"
                    name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
            </div>

                <button className="btn btn-primary" onClick={submitForm}>Create</button>

        </div>
        </div>
    </div>
    )

}

export default SaleForm;
