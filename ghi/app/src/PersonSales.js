import React, { useState, useEffect, } from 'react';

function PersonSales() {
  
  const [salesPersons,setPersons]=useState([]);
  const [empoloyee_id,setValue]=useState('');
  let [total]=useState(0);
  const [sales,setSales]=useState([]);

  const handleChange=(e)=>{setValue(e.target.value); }
  const loadSales= async () => {
    if (empoloyee_id !== '' ){
      const response = await fetch(`http://localhost:8090/api/sales/${ empoloyee_id }/`);
      if (response.ok) {
        let data = await response.json();
        setSales( data.sales );
      }
      else {
        console.log("Error retrieving sales list.")
      }
    }
  }

  useEffect( () => {
    const loadPersons = async () => {   
      const response = await fetch('http://localhost:8090/api/salespersons/');
      if (response.ok) {
        let data = await response.json();
        setPersons( data.sales_persons );
      }
      else {
        console.log("Error retrieving sales persons list.")
      }
    }
    loadPersons();
  }, [] );

    return (
        <>
            <div className="row">
              <div className="offset-0 col-12">
                <div className="shadow p-4 mt-4">
                    <h1> List Sales by Sales Person</h1>
                    <div className="mb-3">  
                      Select: <select onChange={handleChange} required name="empoloyee_id"
                          id="empoloyee_id" className='form-select' value={empoloyee_id}>
                      <option value="">Choose a Sales Person</option>
                      <option value="all">All Sales</option>
                          {salesPersons?.map( person => {
                            return (
                              <option value={person.id} key={person.id}>
                                {person.name} / ID: {person.employee_num}
                              </option>
                            );
                          })}
                      </select><button onClick={loadSales}>Load List</button>
                    </div>

                    <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales?.map( sale => {
                            total = total + sale.price
                            return (
                                <tr key={sale.vin}>
                                    <td>{sale.sales_person}</td>
                                    <td>{sale.customer}</td>
                                    <td>{sale.vin}</td>
                                    <td>${sale.price.toLocaleString()}</td>
                                </tr>
                            )
                        })}
                        <tr>
                            <td></td><td></td><td>TOTAL:</td><td>${total.toLocaleString()}</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </>
      );
    }
    
    
export default PersonSales;