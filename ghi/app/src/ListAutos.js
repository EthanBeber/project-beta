import { useState,useEffect } from 'react';

function AutosList() {

    const [autos,setAutos] = useState([]);
    
    useEffect( () => {
        const loadData = async () => {   
          const response = await fetch('http://localhost:8090/api/autos/');
          if (response.ok) {
            let data = await response.json();
            setAutos( data.automobiles);
          }
          else if (!response.ok) {
            console.log("Error retrieving automobile list.")
          }
        }
        loadData();
      }, [] );


    return (
    <>
        <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Automobile Inventory</h1>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {autos?.map(auto => {
                    return (
                        <tr key={auto.vin}>
                            <td>{auto.vin}</td>
                            <td>{auto.color}</td>
                            <td>{auto.year}</td>
                            <td>{auto.model}</td>
                            <td>{auto.manufacturer}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    
    </>
    )
}

export default AutosList;
