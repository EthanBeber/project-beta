import React, {useState, useEffect} from 'react';

function ModelsList() {
    const [models, setModels] = useState ([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
        else {
            console.log('Could not load model data')
        }
    }

    useEffect(() => {
        fetchData ();
    }, [])

    return (
    <>
    <div className="px-4 py-5 my-1 text-left">
    <h1 className="display-5 fw-bold">Models</h1>
    </div>
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
            </tr>
        </thead>
        <tbody>
            {models.map(model => {
                return (
                    <tr key={model.id}>
                        <td>{model.name}</td>
                        <td>{model.manufacturer.name}</td>
                        <td><img src={model.picture_url} height="75"></img></td>
                    </tr>
                )
            })}
        </tbody>
    </table>
    </>
   )
}

export default ModelsList;
